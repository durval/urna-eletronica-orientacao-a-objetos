#include "CSVReader.hpp"

using namespace std;

CSVReader::CSVReader()
{
    set_fileName("");
    set_delimiter(';');
}

CSVReader::CSVReader(string fileName, char delimiter)
{
    set_fileName(fileName);
    set_delimiter(delimiter);
}

CSVReader::~CSVReader() {}

void CSVReader::set_fileName(string fileName)
{
    this->fileName = fileName;
}

void CSVReader::set_delimiter(char delimiter)
{
    this->delimiter = delimiter;
}

string CSVReader::get_fileName() { return fileName; }

char CSVReader::get_delimiter() { return delimiter; }



/* Método recebe um arquivo CSV e carrega todos os candidados do arquivo nos vectors da urna */
bool CSVReader::load_candidatos(Urna * urna)
{
    /* Vector que irá receber todas as linhas do CSV */
    vector<vector<string>> linhas;

    /* booleano para verificar se a função ñ apresentou erros */
    bool erros;

    /* Método para preenxer o vector linhas com o conteúdo do arquivo csv */
    erros = ler_csv(&linhas);
    if(erros) return 1;

    /* Vector com o incide das colunas selecionadas do arquivo csv */
    vector <int> colunasRelevantes;

    /* Método para preenxer o vector com os indides */
    seleciona_colunas(&colunasRelevantes, &linhas);

    /* Carrega os candidatos na urna */
    load_candidatos_to_urna(&linhas, &colunasRelevantes, urna);

    return 0;
}



/* Método para preenxer o vector linhas com o conteúdo do arquivo csv */
bool CSVReader::ler_csv( vector<vector<string>> * linhas )
{
    /* Diretório padrão onde estão os arquivos csv */
    string diretorio = "dados/";

    /* Recebo o nome do arquivo CSV */
    string file_name = get_fileName();

    /* Concatenação de strings resulta no caminho completo para o arquivo csv */
    string path = diretorio + file_name;

    /* Conversão de 'string' para 'char*' */
    fstream file(path.c_str());

    /* Caso ocorra algum erro para abrir o arquivo  */
    if(!file.is_open()) return 1;

    string buffer;

    while(!file.eof())
    {
        /* Recebo tudo até o \n e coloco no buffer */
        getline(file, buffer);

        /* Coloco o buffer em uma string de strings */
        stringstream ss (buffer);

        /* vector de strings para recber os dados separados pelo delimitador do arquivo csv, neste caso o ';' */
        vector<string> linha;

        while( getline( ss, buffer, get_delimiter() ) )
        {
            /* Remover as aspas excendetes. EX.: ""CARGO"" */
            buffer = buffer.substr(1, buffer.size() - 2);

            linha.push_back(buffer);
        }

        (*linhas).push_back(linha);
    }
    
    file.close();

    return 0;
}



/* Método para preenxer o vector com os indides */
void CSVReader::seleciona_colunas(vector<int>*colunasRelevantes, vector<vector<string>>*linhas)
{
    /* Nessa parte do cógido:

        1. Os métodos substr está sendo usado para remover as       aspas duplas ("") das strings lidas, pois são lidas      dessa maneira, ex.: ""DS_CARGO""
    */

   for (size_t i = 0; i < (*linhas)[0].size(); i++)
    {
        /* string que irá receber o conteudo do vector linhas */
        string nomeCol;

        /* string recebe o conteudo da string */
        nomeCol = (*linhas)[0][i];

        /* Se a string for uma dessas colunas o indice é salvo */
        if (nomeCol == "DS_CARGO" || nomeCol == "NR_CANDIDATO" || nomeCol == "NM_CANDIDATO" || nomeCol == "NM_URNA_CANDIDATO" || nomeCol == "NM_PARTIDO" || nomeCol == "SG_PARTIDO") 
        {
            (*colunasRelevantes).push_back(i);
        }
    }
}



/* Carrega os candidatos na urna */
void CSVReader::load_candidatos_to_urna(vector<vector<string>>*linhas, vector<int>*colunasRelevantes, Urna * urna)
{
    /* Vector de string que irá receber os dados do csv que seram usados para criar os objetos candidatos */
    vector <string> v_cand;

    /* Loop para cada linha do arquivo csv, sendo que cada linha representa os dados de cada candidato */
    for (size_t i = 1; i < (*linhas).size(); i++) 
    {
        /* Removendo os dados dos loops anteriores */
        v_cand.clear();

        /* Loop para cada coluna relevante */
        for (size_t j = 0; j < (*colunasRelevantes).size(); j++) 
        {
            /* Recebe o indice de uma coluna relevante */
            int coluna = (*colunasRelevantes)[j];

            /* string recebe uma coluna relevante de um candidato */
            string texto = (*linhas)[i][coluna];

            /* texto adicionado no vector */
            v_cand.push_back(texto);
        }

        /* É utilizado os dados coletados para criar um objeto candidato */
        Candidatos cand(v_cand[0], v_cand[1], v_cand[2], v_cand[3], v_cand[4], v_cand[5]);

        /* Método que adiciona o candidato criado a um vector de acordo com seu cargo */
        (*urna).adc_candidato(cand);
    }
}