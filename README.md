# Urna Eletrônica para as eleições de 2018 do DF

Este Projeto contém um programa feito em **C++** capaz de simular o funcionamento de uma urna eletrônica brasileira com dados das eleições de 2018 do Distrito Federal.

Para inicializar o programa siga os comandos abaixo:

## Executar o programa

### Instale o copilador (g++)

###### Debian || Ubuntu || Mint
> `sudo apt get g++`

###### macOS
> `g++`

###### Fedora
> `sudo dnf install gcc-c++`

### Limpe o log da camera

> `make clean`

### Compile o programa

> `make all`

### Execute o programa

> `make run`
