#ifndef CSVReader_HPP
#define CSVReader_HPP

#include "urna.hpp"
#include "candidatos.hpp"

#include <string>
#include <vector>
#include <sstream> /* using stringstream */

class CSVReader
{
    private:
        std::string fileName;
        char delimiter;

    public:
        CSVReader();
        CSVReader(std::string fileName, char delimiter=';');
        ~CSVReader();

        void set_fileName(std::string fileName);
        void set_delimiter(char delimiter);

        std::string get_fileName();
        char get_delimiter();

        /* Esse método irá ler o arquivo csv e salvar os candidatos criados nos vectors da urna de acordo com o cargo disputado */
        bool load_candidatos(Urna * urna);

        /* Método para preenxer o vector linhas com o conteúdo do arquivo csv */
        bool ler_csv(std::vector<std::vector<std::string>>*linhas);

        /* Método para preenxer o vector com os indides */
        void seleciona_colunas(std::vector<int>*colunasRelevantes, std::vector<std::vector<std::string>>*linhas);

        /* Carrega os candidatos na urna */
        void load_candidatos_to_urna(std::vector<std::vector<std::string>>*linhas, std::vector<int>*colunasRelevantes, Urna * urna);
};

#endif